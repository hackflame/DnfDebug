#include "Driver.h"
#include "DNFDebug.h"
static DNFDebug g_DNFDebug;

NTSTATUS DriverEntry(PDRIVER_OBJECT pPDriverObj, PUNICODE_STRING pRegistryPath)
{

	pPDriverObj->DriverUnload = UnLoadDriver;
	g_DNFDebug.DNFDebugStart(pPDriverObj);

	return STATUS_SUCCESS;
}

VOID UnLoadDriver(PDRIVER_OBJECT pPDriverObj)
{
	
	g_DNFDebug.UnDebugStart();
}
